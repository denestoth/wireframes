# Midella wireframes

this repo contains the wireframes for the Midella system

the files can be opened with draw.io, available here: https://app.diagrams.net/

wireframes available

* patients: https://mihalycsordas257816.invisionapp.com/public/share/VF1A8N1F4E#/screens/479364441

* providers: https://mihalycsordas257816.invisionapp.com/public/share/VF1A8N1F4E#/screens/479367727